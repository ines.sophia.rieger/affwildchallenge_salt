This submission is subject to the Action Unit detection task of
the Affective Behavior Analysis in-the-wild (ABAW) challenge
at the IEEE Conference on Face and Gesture Recognition 2020.

Team name: SALT

Requirements:

1: use pip or conda to install:
- keras
- tensorflow
- numpy
- pathlib
- Pillow

2: Change the 'path' variable in start_testing_py to your local location of the cropped images folder of the AffWild dataset.
Beware that you might have to use OS-specific folder seperators.

3: Exchange the model you want to test in the 'path_model' variable
model_non-aug.h5 refers to the non-aug model in the paper, model_aug-1.h5 to the aug-1 model.