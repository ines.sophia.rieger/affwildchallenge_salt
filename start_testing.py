import resnet
from keras.layers import Input
from keras.models import Model
import numpy as np
from keras.preprocessing.image import img_to_array, load_img
import pathlib
import os

CONSTANT_NORMALIZING_VALUE = 255


def readImages(query, path):
    images = []
    for i in range(len(query)):
        print(str(path) + str(pathlib.Path(query[i][0][1:len(query)])))
        images.append( img_to_array(load_img(str(path) + str(pathlib.Path(query[i][0][1:len(query)])), grayscale=True )))

    return images

def normalize_Images(imageArray):
    return imageArray / CONSTANT_NORMALIZING_VALUE

def prepare_images(image_data):

    #reshape (n, 112,112 ) -> (n, 112,112,1)
    image_data = np.asarray(image_data, dtype=np.float32)
    image_data_reshaped = np.reshape(image_data, newshape=[-1, 112, 112, 1])

    return image_data_reshaped

def load_images(path, query_path):

    query_np = np.load(query_path, allow_pickle = True)
    query_list = query_np["arr_0"]
    
    return query_list

def predict_images(model, image_data):
    predictions = model.predict(image_data)
    return predictions

def round_with_threshold(myarray, threshold):
    myarray [myarray >= threshold] = 1
    myarray [myarray < threshold] = 0
    myarray = myarray.astype(np.int64)
    return myarray

def write_csv(predictions, save_name):
    with open(save_name, "w") as f:
        f.write("AU1,AU2,AU4,AU6,AU12,AU15,AU20,AU25\n")

    for p in range(predictions.shape[0]):
        with open(save_name, "a+") as f:
            a = str(predictions[p])
            a = a.replace(" ", ",")
            a = a[1:]
            a = a[:-1]
            a = a + "\n"
            f.write(a)

def write_csv_fill(predictions, save_name, lenght_list, img_number_list):
    with open(save_name, "w") as f:
        f.write("AU1,AU2,AU4,AU6,AU12,AU15,AU20,AU25\n")

    position_pred_labels = 0
    for trueframe in range(1,lenght_list+1):
        if trueframe in img_number_list:
            #append true predicted
            with open(save_name, "a+") as f:
                a = str(predictions[position_pred_labels])
                a = a.replace(" ", ",")
                a = a[1:]
                a = a[:-1]
                a = a + "\n"
                f.write(a)
            position_pred_labels += 1
        else:
            #append zeros
            with open(save_name, "a+") as f:
                f.write("0,0,0,0,0,0,0,0\n")

def load_trained_resnet_model(save_path):

    image_size = 112
    batch_size = 1
    num_channel = 1
    block_size = [2,2,2,2]
    action_units= np.array(["AU01","AU02","AU04","AU06","AU12","AU15","AU20","AU25"])

    simpleModel = resnet.Model( image_size , batch_size )
    inputvector_images = Input((image_size, image_size, num_channel), dtype='float32')
    ms_output = simpleModel.model(inputvector_images, action_units, block_size, block_type=resnet.classic_block)
    ms = Model(inputs=[inputvector_images], outputs=ms_output)

    ms.load_weights(save_path)

    return ms

def get_img_list(query_list_element):
    img_list = []
    for imagenr in range(len(query_list_element)):
        img_list.append(int(query_list_element[imagenr][0][-9:-4]))
    return img_list


if __name__ == "__main__":
    #path = "D:\\Datasets\\AffWild2Challenge\\Data"
    path = "/datapath"

    path_model = "model_aug-1.h5" # "model_non-aug.h5" or "model_aug-1.h5"
    query_path = 'list_all_querys.npz'

    query_list = load_images(path, query_path)
    print("number of persons: ", len(query_list))

    list_of_video_names = ['30-30-1920x1080_left',\
    '30-30-1920x1080_right','39-25-424x240','48-30-720x1280','6-30-1920x1080_right','69-25-854x480',
    '7-60-1920x1080','74-25-1920x1080','79-30-960x720','8-30-1280x720', '82-25-854x480','88-30-360x480',\
    'video25','video28']

    list_of_max_frames = [3063, 3063, 4591, 1591, 7986, 18782, 23149, 5042, 2495, 9114, 5403, 3723, 3645, 8728]

    list_of_sums = []

    for elem in range(query_list.shape[0]):
        img_number_list = get_img_list(query_list[elem])

        images_list = readImages(query_list[elem], path)
        image_data_preprocessed = prepare_images(images_list)
        image_data_normalized = normalize_Images(image_data_preprocessed)
        print(image_data_normalized.shape)

        model = load_trained_resnet_model(path_model)
        predictions = predict_images(model, image_data_normalized)
        print(predictions)
        predictions_rounded = round_with_threshold(predictions, 0.5)
        print(sum(predictions_rounded))
        list_of_sums.append(sum(predictions_rounded))
        save_name = list_of_video_names[elem] + ".txt"
        write_csv_fill(predictions_rounded, save_name, list_of_max_frames[elem], img_number_list)

    print(list_of_sums)