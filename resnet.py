"""
By Ines Rieger
Script for training various ResNet models based on
He, Kaiming, et al. "Deep residual learning for image recognition." Proceedings of the IEEE conference on computer vision and pattern recognition. 2016.
"""
from keras import layers, backend, initializers
import keras
import math

print ( "Keras Version" , keras.__version__ )


operational_seed = 3

'''MODEL LAYERS'''
def conv2d(input , filters , kernel_size, stride, padding = 'same'):
    model = layers.Conv2D ( filters , kernel_size , strides = stride , padding = padding ,
                            kernel_initializer=initializers.glorot_normal(seed=operational_seed),
                            bias_initializer = 'zeros' ) ( input )
    return model

def batch_norm (input):

    return layers.BatchNormalization(
    axis=-1,
    momentum=0.99,
    epsilon=0.001)(input)


def max_pool(input, ksize, strides, padding):

    return layers.MaxPool2D(
    ksize,
    strides,
    padding = padding,
    data_format='channels_last',
    name=None)(input)

def activation (input):
    return layers.Activation('relu')(input) 

def bn_activation(input):

    bn = batch_norm(input)
    return activation(bn)

def dense_layer(input , filters):
    model = layers.Dense ( filters) ( input )
    return model


#upgraded kernel_size to 3x3
def projection_shortcut(input, filters, strides = [2,2], kernel_size = [3,3]):
    return conv2d(input, filters, kernel_size, strides, padding = 'same')

def classic_block(input, filters, stride, is_projection = False):
    """
    Classic residual block described first in arXiv:1512.03385, combined with preactivation  from arXiv: 1603.05027
    """
    # identity shortcut
    shortcut = input

    preactivation = bn_activation(input)

    #projection shortcut for first block of stack, the stride of the first layer is [2,2] to decrease feature map size
    # projection shortcut after batch normalization like in official tensorflow implementation
    if is_projection:
        shortcut = projection_shortcut(preactivation, filters)

    conv_1 = conv2d(preactivation, filters, [3,3], stride)

    preactivation = bn_activation(conv_1)
    conv_2 = conv2d(preactivation, filters, [3,3], [1,1])

    conv_2_conc = layers.Add()([conv_2, shortcut])

    return conv_2_conc

class Model():

    def __init__(self , image_size , batch_size):
        """

        :param image_size: Image size of input faces
        """
        self.image_size = image_size
        self.batch_size = batch_size

    '''BUILD MODEL'''
    #reuse = None for training, reuse = True for testing
    # is_training = True when training, is_training = False when testing
    def model(self,  inputvector_images, output_classes, block_size, block_type):


        print("Starting Graph creation...")
        #print("image batch", image_batch.get_shape())

        #First Convolution Layer
        model = conv2d(inputvector_images, 64, kernel_size= [7,7], stride= [1,1])
        model = max_pool(model, ksize=[3,3], strides= [2,2], padding='same')
        #print("after first conv and pooling", model.get_shape())

        #First residual Block
        for i in range(0, block_size[0]):
            #print("First res block", i)
            #downsamples, if first block of block
            model = block_type(model, 64, stride = [1,1])
        #print("after first block", model.get_shape())

        #Second residual Block
        for i in range(0, block_size[1]):
            #print("Second res block", i)
            #downsamples, if first block of block
            model = block_type(model, 128, stride = ([2,2] if i == 0 else [1,1]), is_projection = True if i == 0 else False)
        #print("after second block", model.get_shape())

        #Third residual Block
        for i in range(0, block_size[2]):
            #print("Third res block", i)
            #downsamples, if first block of block
            model = block_type(model, 256, stride=([2,2] if i == 0 else [1,1]), is_projection = True if i == 0 else False)
         #print("after third block", model.get_shape())

        #Fourth residual Block
        for i in range(0, block_size[3]):
            #print("Fourth res block", i)
            # downsamples, if first block of block
            model = block_type(model, 512, stride=([2,2] if i == 0 else [1,1]), is_projection = True if i == 0 else False)

        #print("after fourth block", model.get_shape())
        model = bn_activation(model)

        model = layers.AveragePooling2D( pool_size=7, strides=1, padding='VALID')(model)
        #print("average pooling", model.get_shape())

        model = layers.Flatten()(model)

        model = dense_layer(model, 1024)

        model = activation(model)

        output = layers.Dense( output_classes.shape[0])(model)

        output = layers.Activation('sigmoid', name = "sigmoid")(output)

        return output